const express = require("express");
const router = express.Router();
const helpers = require('../util/helpers');
const seachController = require('../controllers/search');
const cache = require ('../util/cacheStr');
const { buildCheckFunction } = require('express-validator/check');
const checkQuery = buildCheckFunction(['query']);

router.get('/search', [
    checkQuery('movie').trim().not().isEmpty()
], cache(10), helpers, seachController.search);

module.exports = router;