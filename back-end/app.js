const express = require('express');
const bodyParser = require('body-parser');
const helmet= require('helmet');
const compression= require('compression');
const searchRoutes= require('./routes/search');
const app = express();
// const https = require('https');
// const http = require('http');
var fs = require('fs');
// const options = {
//     key: fs.readFileSync('./https/key.pem'),
//     cert: fs.readFileSync('./https/cert.pem')
//   };

app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
      return res.sendStatus(200);
    }
    next();
  });
app.use(searchRoutes);
app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
  });

  app.listen(process.env.PORT || 3000);

// Used for local development as heroku has it's own https configuration
// http.createServer(app).listen(process.env.PORT || 3000, () => {
//     console.log("Server running on port 3000");
// });
// https.createServer(options, app).listen(process.env.PORT || 3001, () => {
//     console.log("Server running on port 3001");
// });