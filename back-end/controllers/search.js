exports.search = async(req, res, next) => {
    try{
        const youtube = req.result.youtube.items;
        const movieDB = req.result.movieDB;
        // setTimeout(() => {
            res.status(200).json({
                message: 'Fetched successfully.',
                status: res.statusCode,
                youTube: youtube,
                movieDB: movieDB
    
            });
        //   }, 5000) //setTimeout was used to simulate slow processing request
    }catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
    }
}
