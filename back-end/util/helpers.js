const config = require('../util/config');
const fetch = require('node-fetch');
const { validationResult } = require('express-validator/check');

module.exports = async(req, res, next)=>{
    try{
      const errors = validationResult(req);
      if(!errors.isEmpty()){
        const error = new Error('Validation failed.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
      const str = req.query.movie;
      const find = await findBySearch(str);
         req.result = find;
         next();
    }catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
    }
};

findBySearch = (str) =>{
 return  new Promise((resolve, reject)=>{
  fetch('https://www.googleapis.com/youtube/v3/search?part=snippet&q='+str+'-trailer&start-index=1&max-results=1&v=2&alt=json&hd&type=video&key='+config.YOUTUBE_API3)
  .then(res => res.json())
  .then(json => { return json;}).then((youtube) =>{
    fetch('https://api.themoviedb.org/3/search/movie?include_adult=false&page=1&language=en-US&api_key='+config.MOVIE_DB+'&query='+str)
    .then(res => res.json())
    .then(json => { return json; }).then((movieDB)=>{
      const movies = movieDB.results;
      const promises = [];
      for(let movie of movies){
        promises.push(getYouTubeId(movie));
      }
      Promise.all(promises).then(data => {
        const result = {youtube: youtube, movieDB: data};
        resolve(result);
      }).catch(err => reject(err));
    });
  });
 });
}

getYouTubeId = (item)=>{
  return new Promise((resolve, reject)=>{
    fetch('https://api.themoviedb.org/3/movie/'+item.id+'/videos?api_key=cf3ca29c756a457758abfd88437393cc&language=en-US')
    .then(res => res.json())
    .then(result => {
      item.youTubeData = result.results;
      resolve(item);
    }).catch(err => reject(err));
  });
}

