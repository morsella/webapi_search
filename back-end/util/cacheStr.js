const memoryCache = require('memory-cache');

const cacheStr = (duration) => {
    return (req, res, next) => {
      let key = '__movie_search_cache__' + req.originalUrl || req.url;
      console.log(key);
      let cachedBody = memoryCache.get(key);
      if (cachedBody) {
        res.send(cachedBody);
        return;
      } else {
        res.sendResponse = res.send
        res.send = (body) => {
          memoryCache.put(key, body, duration * 1000);
          res.sendResponse(body);
        }
        next();
      }
    }
  }
  module.exports = cacheStr;