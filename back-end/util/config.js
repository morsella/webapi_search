/*
* Create and export configuration variables
*/
//Container for all the environments
var environments = {};

//Setting up default staging object
environments.staging = {
    'httpPort' : 3000,
    'httpsPort': 3001,
  'envName': 'staging',

};

environments.production = {
  'envName': 'production',

};

//Which environment was passed as a command-line argument
var currentEnvironment  = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

//Check that the current environment is defined above
// If not, default staging env
var environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

//Export module
module.exports = environmentToExport;