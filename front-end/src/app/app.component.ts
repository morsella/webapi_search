import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import  { DataContextService } from './data-context';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  constructor(@Inject(DataContextService) private data: DataContextService, private sanitizer: DomSanitizer){}
  title = 'place queens';
  board = [];
  errors = {
    input: null,
    youtube: null,
    movieDB: null
  };
  youTube = [];
  movieDB = [];
  ngOnInit(){
    // let n = 7;
    // this.placeQueens(n);
  }
  sinitizeUrl(url: string, param: string){
    let src = url+param+'?autoplay=0&enablejsapi=1&origin=http://example.com';
   return this.sanitizer.bypassSecurityTrustResourceUrl(src);
  }
  onSubmit(form: NgForm){
    if(form.value.movie.trim() === ''){ this.errors.input = 'Input can\'t be empty'; return;}
    this.data.getWithProgress('/search?movie='+form.value.movie).then((data)=>{
      console.log(data);
      if(data.youTube.length === 0)this.errors.youtube = 'No results for YouTube with this title';
      if(data.movieDB.length === 0)this.errors.movieDB = 'No results for The Movie DB with this title';
      this.youTube = data.youTube;
      this.movieDB = data.movieDB;
      this.errors.input = null;
    });
  }
